%define name        jami-daemon
%define version     RELEASE_VERSION
%define release     0

Name:          %{name}
Version:       %{version}
Release:       %{release}%{?dist}
Summary:       Daemon component of Jami
Group:         Applications/Internet
License:       GPLv3+
Vendor:        Savoir-faire Linux
URL:           https://jami.net/
Source:        %{name}-%{version}.tar.gz
Patch0:        revert-ddbe4b088f.diff
Requires:      jami-daemon = %{version}

# Build dependencies
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: cmake
BuildRequires: gcc-c++
BuildRequires: gettext-devel
BuildRequires: libtool
BuildRequires: make
BuildRequires: which
BuildRequires: yasm
BuildRequires: git

# Build and runtime dependencies.  Requires directives are
# automatically made to linked shared libraries via RPM, so there's no
# need to explicitly relist them.
%if 0%{?fedora} >= 32
BuildRequires: NetworkManager-libnm-devel
BuildRequires: dbus-devel
BuildRequires: expat-devel
BuildRequires: opus-devel
BuildRequires: pulseaudio-libs-devel
%endif
%if %{defined suse_version}
BuildRequires: libdbus-c++-devel
BuildRequires: libexpat-devel
BuildRequires: libopus-devel
BuildRequires: libpulse-devel
%endif
BuildRequires: alsa-lib-devel
#BuildRequires: gnutls-devel
BuildRequires: jsoncpp-devel
#BuildRequires: libXext-devel
#BuildRequires: libXfixes-devel
BuildRequires: libuuid-devel
#BuildRequires: libva-devel
#BuildRequires: libvdpau-devel
BuildRequires: pcre-devel
#BuildRequires: uuid-devel
BuildRequires: yaml-cpp-devel
# SailfishOS:
#BuildRequires: meson
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: asio-devel
BuildRequires: dbus-cxx-tools
BuildRequires: dbus-devel
BuildRequires: ffmpeg-devel
BuildRequires: libgcrypt-devel
BuildRequires: gmp-devel
BuildRequires: http-parser-devel >= 2.9.3
BuildRequires: kernel-headers
BuildRequires: libarchive-devel
BuildRequires: libnatpmp-devel
BuildRequires: opus-devel
BuildRequires: pulseaudio-devel
BuildRequires: restinio-devel
BuildRequires: systemd-devel
BuildRequires: systemd-libs
BuildRequires: pkgconfig(dbus-c++-1)
BuildRequires: pkgconfig(dbus-cxx-2.0)
BuildRequires: pkgconfig(expat)
BuildRequires: pkgconfig(fmt) >= 5.3.0
BuildRequires: pkgconfig(gnutls) = 3.6.7
BuildRequires: pkgconfig(jsoncpp)
BuildRequires: pkgconfig(libgit2) >= 1.1.0
BuildRequires: pkgconfig(libpjproject)
BuildRequires: pkgconfig(libsecp256k1)
#                        nettle 3.4 required? contrib has git rev "c180b4d7afbda4049ad265d1366567f62a7a4a3a"
#BuildRequires: pkgconfig(nettle) = 3.4.1
BuildRequires: pkgconfig(nettle) = 3.5.1
BuildRequires: pkgconfig(msgpack) >= 3.2.0
BuildRequires: pkgconfig(libupnp) = 1.8.4
BuildRequires: pkgconfig(opendht) = 2.2.0
BuildRequires: pkgconfig(openssl) >= 1.1.1
BuildRequires: pkgconfig(speex)
BuildRequires: pkgconfig(speexdsp)
BuildRequires: pkgconfig(webrtc-audio-processing) = 0.3.1



%description
This package contains the daemon of Jami, a free software for
universal communication which respects the freedoms and privacy of its
users.

%prep
%setup -n %{name}-%{version}/upstream
%patch0 -p1

%build
# Configure the Jami bundled libraries (ffmpeg & pjproject).
mkdir -p daemon/contrib/native
pushd daemon/contrib/native && \
    ../bootstrap \
        --no-checksums \
        --disable-ogg \
        --disable-flac \
        --disable-vorbis \
        --disable-vorbisenc \
        --disable-speex \
        --disable-sndfile \
        --disable-gsm \
        --disable-speexdsp \
        --disable-natpmp && \
    make list && \
    make fetch && \
    make %{_smp_mflags} V=1 && \
    make %{_smp_mflags} V=1 .ffmpeg
popd
# Configure the daemon.
pushd daemon && \
    ./autogen.sh && \
    %configure \
        --disable-shared

# Build the daemon.
make %{_smp_mflags} V=1
#pod2man %{_builddir}/ring-project/daemon/man/dring.pod \
#        > %{_builddir}/ring-project/daemon/dring.1
popd

%install
#DESTDIR=%%{buildroot} make -C daemon install
pushd daemon
%make_install
cp %{_builddir}/ring-project/daemon/dring.1 \
   %{buildroot}/%{_mandir}/man1/dring.1
rm -rfv %{buildroot}/%{_libdir}/*.a
rm -rfv %{buildroot}/%{_libdir}/*.la

%files
%defattr(-,root,root,-)
%{_libdir}/ring/dring
%{_datadir}/ring/ringtones
%{_datadir}/dbus-1/services/*
%{_datadir}/dbus-1/interfaces/*
%doc %{_mandir}/man1/dring*

%package devel
Summary: Development files of the Jami daemon

%description devel
This package contains the header files for using the Jami daemon as a library.

%files devel
%{_includedir}/dring
%{_includedir}/jami_contact.h

%post
/sbin/ldconfig

%postun
/sbin/ldconfig
